<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ngpcampaign_home_ctools_plugin_api() {
  module_load_include('inc', 'ngpcampaign_home', 'ngpcampaign_home.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_home_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_node_info().
 */
function ngpcampaign_home_node_info() {
  module_load_include('inc', 'ngpcampaign_home', 'ngpcampaign_home.features.node');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_home_node_info', $args);
}

/**
 * Implementation of hook_default_page_manager_pages().
 */
function ngpcampaign_home_default_page_manager_pages() {
  module_load_include('inc', 'ngpcampaign_home', 'ngpcampaign_home.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_home_default_page_manager_pages', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function ngpcampaign_home_views_default_views() {
  module_load_include('inc', 'ngpcampaign_home', 'ngpcampaign_home.features.views');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_home_views_default_views', $args);
}
