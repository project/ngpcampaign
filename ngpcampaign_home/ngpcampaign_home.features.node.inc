<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _ngpcampaign_home_node_info() {
  $items = array(
    'action' => array(
      'name' => t('Action Item'),
      'module' => 'features',
      'description' => t('An action item which will appear in a special block on the homepage'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
