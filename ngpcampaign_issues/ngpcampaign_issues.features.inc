<?php

/**
 * Implementation of hook_content_default_fields().
 */
function ngpcampaign_issues_content_default_fields() {
  module_load_include('inc', 'ngpcampaign_issues', 'ngpcampaign_issues.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_issues_content_default_fields', $args);
}

/**
 * Implementation of hook_node_info().
 */
function ngpcampaign_issues_node_info() {
  module_load_include('inc', 'ngpcampaign_issues', 'ngpcampaign_issues.features.node');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_issues_node_info', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function ngpcampaign_issues_views_default_views() {
  module_load_include('inc', 'ngpcampaign_issues', 'ngpcampaign_issues.features.views');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_issues_views_default_views', $args);
}
