<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _ngpcampaign_issues_node_info() {
  $items = array(
    'issue' => array(
      'name' => t('Issue Statement'),
      'module' => 'features',
      'description' => t('A statement of the campaign\'s position on an issue'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
