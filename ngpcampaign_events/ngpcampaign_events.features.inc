<?php

/**
 * Implementation of hook_content_default_fields().
 */
function ngpcampaign_events_content_default_fields() {
  module_load_include('inc', 'ngpcampaign_events', 'ngpcampaign_events.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_events_content_default_fields', $args);
}

/**
 * Implementation of hook_node_info().
 */
function ngpcampaign_events_node_info() {
  module_load_include('inc', 'ngpcampaign_events', 'ngpcampaign_events.features.node');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_events_node_info', $args);
}

/**
 * Implementation of hook_user_default_permissions().
 */
function ngpcampaign_events_user_default_permissions() {
  module_load_include('inc', 'ngpcampaign_events', 'ngpcampaign_events.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_events_user_default_permissions', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function ngpcampaign_events_views_default_views() {
  module_load_include('inc', 'ngpcampaign_events', 'ngpcampaign_events.features.views');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_events_views_default_views', $args);
}
