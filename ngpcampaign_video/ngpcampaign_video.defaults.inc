<?php

/**
 * Helper to implementation of hook_user_default_permissions().
 */
function _ngpcampaign_video_user_default_permissions() {
  $permissions = array();

  // Exported permission: create video content
  $permissions[] = array(
    'name' => 'create video content',
    'roles' => array(),
  );

  // Exported permission: delete any video content
  $permissions[] = array(
    'name' => 'delete any video content',
    'roles' => array(),
  );

  // Exported permission: delete own video content
  $permissions[] = array(
    'name' => 'delete own video content',
    'roles' => array(),
  );

  // Exported permission: edit any video content
  $permissions[] = array(
    'name' => 'edit any video content',
    'roles' => array(),
  );

  // Exported permission: edit own video content
  $permissions[] = array(
    'name' => 'edit own video content',
    'roles' => array(),
  );

  return $permissions;
}
