<?php

/**
 * Implementation of hook_node_info().
 */
function ngpcampaign_video_node_info() {
  module_load_include('inc', 'ngpcampaign_video', 'ngpcampaign_video.features.node');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_video_node_info', $args);
}

/**
 * Implementation of hook_user_default_permissions().
 */
function ngpcampaign_video_user_default_permissions() {
  module_load_include('inc', 'ngpcampaign_video', 'ngpcampaign_video.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_video_user_default_permissions', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function ngpcampaign_video_views_default_views() {
  module_load_include('inc', 'ngpcampaign_video', 'ngpcampaign_video.features.views');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_video_views_default_views', $args);
}
