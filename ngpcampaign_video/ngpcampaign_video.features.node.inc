<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _ngpcampaign_video_node_info() {
  $items = array(
    'video' => array(
      'name' => t('Video'),
      'module' => 'features',
      'description' => t('A video from the campaign hosted by a service like YouTube, Vimeo, or Google Videos'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
