<?php

/**
 * Implementation of hook_content_default_fields().
 */
function ngpcampaign_media_content_default_fields() {
  module_load_include('inc', 'ngpcampaign_media', 'ngpcampaign_media.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_media_content_default_fields', $args);
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ngpcampaign_media_ctools_plugin_api() {
  module_load_include('inc', 'ngpcampaign_media', 'ngpcampaign_media.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_media_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_node_info().
 */
function ngpcampaign_media_node_info() {
  module_load_include('inc', 'ngpcampaign_media', 'ngpcampaign_media.features.node');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_media_node_info', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function ngpcampaign_media_views_default_views() {
  module_load_include('inc', 'ngpcampaign_media', 'ngpcampaign_media.features.views');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_media_views_default_views', $args);
}

/**
 * Implementation of hook_default_page_manager_pages().
 */
function ngpcampaign_media_default_page_manager_pages() {
  module_load_include('inc', 'ngpcampaign_media', 'ngpcampaign_media.defaults');
  $args = func_get_args();
  return call_user_func_array('_ngpcampaign_media_default_page_manager_pages', $args);
}
