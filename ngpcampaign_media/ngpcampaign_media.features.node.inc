<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _ngpcampaign_media_node_info() {
  $items = array(
    'news' => array(
      'name' => t('News Article'),
      'module' => 'features',
      'description' => t('A news article about the campaign from an outside source'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'press' => array(
      'name' => t('Press Release'),
      'module' => 'features',
      'description' => t('A press release from the campaign'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
